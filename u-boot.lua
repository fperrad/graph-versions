#!/usr/bin/env lua

local lfs = require'lfs'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return k, t[k]
        end
    end
end

if not lfs.attributes'u-boot' then
    os.execute'git clone https://github.com/u-boot/u-boot.git'
else
    os.execute'cd u-boot && git pull --no-rebase'
end

local data = {}
for line in io.popen'cd u-boot && git log --tags --pretty=format:"%ai %d"':lines() do
    local year, month, day, tags = line:match'^(%d%d%d%d)%-(%d%d)%-(%d%d) %d%d:%d%d:%d%d [^ ]+%s+%(([^)]+)%)'
    if tags then
        for tag in tags:gmatch'([^,]*),?' do
            local v1 = tag:match'tag: v(20[0-9][0-9]%.%d%d+)$'
            if v1 then
--                print(v1)
                data[v1] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
            end
        end
    end
end

local img = cp.new(1200, 800)
local start_date = 2008

local xTickLabels = {}
local yTickLabels = {}
for minor, date in sorted_pairs(data) do
    local lbl = minor
    local xdata = {}
    local ydata = {}
    local tags = {}
    local year = math.floor(date)
    xTickLabels[year - start_date] = tostring(year)
    xdata[#xdata+1] = date - start_date
    ydata[#ydata+1] = date - start_date
    tags[#tags+1] = false
    tags[#tags] = lbl
    img:setData(xdata, ydata, 'blue')
    img:setTag(tags)
end

img:setGraphOptions{
    title               = 'Releases of U-Boot',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    vertGraphOffset     = 15,
}

local f = assert(io.open('u-boot.png', 'wb'))
f:write(img:draw())
f:close()
