#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);

my $uClibc = {};
foreach ('https://www.uclibc.org/downloads/', 'https://www.uclibc.org/downloads/old-releases/') {
    my $response = $ua->get($_);
    die $response->status_line unless $response->is_success;
    for (split "\n", $response->decoded_content) {
        if (/>uClibc-0\.9\.(\d+)\.?(\d+)?\.tar\.bz2<\/a>\s+(\d{4})-(\d{2})-(\d{2})/ && $1 >= 27) {
            my $date = DateTime->new(
                year => $3,
                month => $4,
                day => $5,
            );
            $uClibc->{$1}->{$2||'0'} = $date;
        }
    }
}
my $uClibc_ng = {};
foreach ('https://downloads.uclibc-ng.org/releases/') {
    my $response = $ua->get($_);
    die $response->status_line unless $response->is_success;
    for (split "\n", $response->decoded_content) {
        if (/>1\.(\d+)\.(\d+)\/<[^>]+><[^>]+><[^>]+>(\d{4})-(\d{2})-(\d{2})/) {
            my $date = DateTime->new(
                year => $3,
                month => $4,
                day => $5,
            );
            $uClibc_ng->{$1}->{$2} = $date;
        }
    }
}

my $img = Chart::Plot::Tagged->new(1200, 480);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$uClibc}) {
    my $lbl = '0.9.' . $major;
    $yTickLabels{$y} = $lbl;
    my $set = $uClibc->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2005} = '' . $date->year;
        push @xdata, $date->year - 2005 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    $lbl = '0.9.' . $major;
    $lbl .= '.' . $max if $max > 0;
    push @tags, $lbl;
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
}

for my $major (sort {$a <=> $b} keys %{$uClibc_ng}) {
    my $lbl = '1.' . $major;
    $yTickLabels{$y} = $lbl;
    my $set = $uClibc_ng->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2005} = '' . $date->year;
        push @xdata, $date->year - 2005 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, '1.' . $major . '.' . $max;
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of uClibc / uClibc-ng',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'uClibc.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

