#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);
my $response = $ua->get('https://download.edgewall.org/trac/');
die $response->status_line unless $response->is_success;

my $trac = {};
for (split "\n", $response->decoded_content) {
    if (/>[Tt]rac-(0\.1\d|1\.[024])\.?(\d+)?\.tar\.gz<[^>]+><[^>]+><[^>]+>(\d{4})-(\d{2})-(\d{2}) /) {
        my $date = DateTime->new(
            year => $3,
            month => $4,
            day => $5,
        );
        $trac->{$1}->{$2||'0'} = $date;
    }
}

my $img = Chart::Plot::Tagged->new(1200, 480);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$trac}) {
    $yTickLabels{$y} = $major;
    my $set = $trac->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2006} = '' . $date->year;
        push @xdata, $date->year - 2006 + $date->day_of_year/365;
        push @ydata, $y;
        my $lbl = $major;
        $lbl .= '.' . $minor if $minor > 0;
        push @tags, $lbl;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, undef;
    $img->setData(\@xdata, \@ydata, 'blue up');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of Trac',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'trac.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

