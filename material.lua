#!/usr/bin/env lua

local http = require'ssl.https'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return i, k, t[k]
        end
    end
end

local content = assert(http.request'https://raw.githubusercontent.com/angular/material/master/CHANGELOG.md')
local data = {}
for line in content:gmatch'([^\n]*)\n?' do
    local minor, patch, rc, year, month, day
    minor, patch, rc, year, month, day = line:match'(%d%.%d+)%.(%d+)([^%]]*)%]%([^%)]+%)[%s%w%-]+%((%d+)%-(%d+)%-(%d+)%)'
    if day and rc:sub(1, 1) ~= '-' then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][tonumber(patch)] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
    minor, patch, year, month, day = line:match'# (1%.%d+)%.(%d+) %((%d+)%-(%d+)%-(%d+)'
    if day then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][tonumber(patch)] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
end

local img = cp.new(1200, 480)
local start_date = 2015

local xTickLabels = {}
local yTickLabels = {}
for y, minor, set in sorted_pairs(data) do
    yTickLabels[y]= minor
    local xdata = {}
    local ydata = {}
    local tags = {}
    local max_patch
    for _, patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - start_date] = tostring(year)
        xdata[#xdata+1] = date - start_date
        ydata[#ydata+1] = y
        tags[#tags+1] = false
        max_patch = patch
    end
    local lbl = minor .. '.' .. max_patch
    tags[#tags] = lbl
    img:setData(xdata, ydata, 'blue')
    img:setTag(tags)
end

img:setGraphOptions{
    title               = 'Releases of Material',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    vertGraphOffset     = 15,
}

local f = assert(io.open('material.png', 'wb'))
f:write(img:draw())
f:close()
