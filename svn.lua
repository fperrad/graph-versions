#!/usr/bin/env lua

local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return i, k, t[k]
        end
    end
end

-- see http://svn.apache.org/repos/asf/subversion/trunk/CHANGES
local data = {
    [0] = {
        [0] = date{year = 2004, month =  2, day =  23},
        [1] = date{year = 2004, month =  3, day =  12},
        [2] = date{year = 2004, month =  4, day =  15},
        [3] = date{year = 2004, month =  5, day =  19},
        [4] = date{year = 2004, month =  5, day =  21},
        [5] = date{year = 2004, month =  6, day =  10},
        [6] = date{year = 2004, month =  7, day =  19},
        [7] = date{year = 2004, month =  9, day =  17},
        [8] = date{year = 2004, month =  9, day =  22},
        [9] = date{year = 2004, month = 10, day =  13},
    },
    [1] = {
        [0] = date{year = 2004, month =  9, day =  29},
        [1] = date{year = 2004, month = 10, day =  22},
        [2] = date{year = 2004, month = 12, day =  20},
        [3] = date{year = 2005, month =  1, day =  14},
        [4] = date{year = 2005, month =  4, day =   1},
    },
    [2] = {
        [0] = date{year = 2005, month =  5, day =  21},
        [1] = date{year = 2005, month =  7, day =   5},
        [3] = date{year = 2005, month =  8, day =  19},
    },
    [3] = {
        [0] = date{year = 2005, month = 12, day =  30},
        [1] = date{year = 2006, month =  3, day =  25},
        [2] = date{year = 2006, month =  5, day =  23},
    },
    [4] = {
        [0] = date{year = 2006, month =  9, day =  10},
        [2] = date{year = 2006, month = 11, day =   2},
        [3] = date{year = 2007, month =  1, day =  18},
        [4] = date{year = 2007, month =  5, day =  30},
        [5] = date{year = 2007, month =  8, day =  27},
        [6] = date{year = 2007, month = 12, day =  21},
    },
    [5] = {
        [0] = date{year = 2008, month =  6, day =  19},
        [1] = date{year = 2008, month =  7, day =  26},
        [2] = date{year = 2008, month =  8, day =  30},
        [3] = date{year = 2008, month = 10, day =  10},
        [4] = date{year = 2008, month = 10, day =  24},
        [5] = date{year = 2008, month = 12, day =  22},
        [6] = date{year = 2009, month =  2, day =  26},
        [7] = date{year = 2009, month =  8, day =   6},
        [9] = date{year = 2010, month = 12, day =   6},
    },
    [6] = {
        [0] = date{year = 2009, month =  3, day =  20},
        [1] = date{year = 2009, month =  4, day =   9},
        [2] = date{year = 2009, month =  5, day =  11},
        [3] = date{year = 2009, month =  6, day =  22},
        [4] = date{year = 2009, month =  8, day =   6},
        [5] = date{year = 2009, month =  8, day =  22},
        [6] = date{year = 2009, month = 10, day =  22},
        [9] = date{year = 2010, month =  1, day =  25},
        [11]= date{year = 2010, month =  4, day =  19},
        [12]= date{year = 2010, month =  6, day =  21},
        [13]= date{year = 2010, month = 10, day =   1},
        [15]= date{year = 2010, month = 11, day =  26},
        [16]= date{year = 2011, month =  3, day =   2},
        [17]= date{year = 2011, month =  6, day =   1},
        [18]= date{year = 2012, month =  3, day =  29},
        [19]= date{year = 2012, month =  9, day =  10},
        [20]= date{year = 2013, month =  1, day =   4},
        [21]= date{year = 2013, month =  4, day =   4},
        [23]= date{year = 2013, month =  5, day =  30},
    },
    [7] = {
        [0] = date{year = 2011, month = 10, day =  11},
        [1] = date{year = 2011, month = 10, day =  24},
        [2] = date{year = 2011, month = 12, day =   2},
        [3] = date{year = 2012, month =  2, day =  14},
        [4] = date{year = 2012, month =  3, day =   8},
        [5] = date{year = 2012, month =  5, day =  17},
        [6] = date{year = 2012, month =  8, day =  15},
        [7] = date{year = 2012, month = 10, day =   9},
        [8] = date{year = 2012, month = 12, day =  17},
        [9] = date{year = 2013, month =  4, day =   4},
        [10]= date{year = 2013, month =  5, day =  30},
        [11]= date{year = 2013, month =  7, day =  24},
        [13]= date{year = 2013, month =  8, day =  30},
        [14]= date{year = 2013, month = 11, day =  25},
        [16]= date{year = 2014, month =  2, day =  26},
        [17]= date{year = 2014, month =  5, day =   7},
        [18]= date{year = 2014, month =  8, day =  11},
        [19]= date{year = 2014, month = 12, day =  15},
        [20]= date{year = 2015, month =  3, day =  31},
        [21]= date{year = 2015, month =  8, day =   5},
        [22]= date{year = 2015, month = 12, day =  15},
    },
    [8] = {
        [0] = date{year = 2013, month =  6, day =  18},
        [1] = date{year = 2013, month =  7, day =  24},
        [3] = date{year = 2013, month =  8, day =  30},
        [4] = date{year = 2013, month = 10, day =  29},
        [5] = date{year = 2013, month = 11, day =  25},
        [8] = date{year = 2014, month =  2, day =  19},
        [9] = date{year = 2014, month =  5, day =   7},
        [10]= date{year = 2014, month =  8, day =  11},
        [11]= date{year = 2014, month = 12, day =  15},
        [13]= date{year = 2015, month =  3, day =  31},
        [14]= date{year = 2015, month =  8, day =   5},
        [15]= date{year = 2015, month = 12, day =  15},
        [16]= date{year = 2016, month =  4, day =  28},
        [17]= date{year = 2016, month = 11, day =  29},
        [18]= date{year = 2017, month =  7, day =  10},
        [19]= date{year = 2017, month =  8, day =  10},
    },
    [9] = {
        [0] = date{year = 2015, month =  8, day =   5},
        [1] = date{year = 2015, month =  9, day =   2},
        [2] = date{year = 2015, month =  9, day =  30},
        [3] = date{year = 2015, month = 12, day =  15},
        [4] = date{year = 2016, month =  4, day =  28},
        [5] = date{year = 2016, month = 11, day =  29},
        [6] = date{year = 2017, month =  7, day =   5},
        [7] = date{year = 2017, month =  8, day =  10},
        [9] = date{year = 2018, month =  7, day =  20},
        [10]= date{year = 2019, month =  1, day =  11},
        [12]= date{year = 2019, month =  7, day =  24},
    },
    [10] = {
        [0] = date{year = 2018, month =  4, day =  13},
        [2] = date{year = 2018, month =  7, day =  20},
        [3] = date{year = 2018, month = 10, day =  10},
        [4] = date{year = 2019, month =  1, day =  11},
        [6] = date{year = 2019, month =  7, day =  24},
        [7] = date{year = 2021, month =  2, day =  10},
        [8] = date{year = 2022, month =  4, day =  12},
    },
    [11] = {
        [0] = date{year = 2018, month = 10, day =  30},
        [1] = date{year = 2019, month =  1, day =  11},
    },
    [12] = {
        [0] = date{year = 2019, month =  4, day =  12},
        [2] = date{year = 2019, month =  7, day =  24},
    },
    [13] = {
        [0] = date{year = 2019, month = 10, day =  30},
    },
    [14] = {
        [0] = date{year = 2020, month =  5, day =  27},
        [1] = date{year = 2021, month =  2, day =  10},
        [2] = date{year = 2022, month =  4, day =  12},
        [3] = date{year = 2023, month = 12, day =  20},
        [4] = date{year = 2024, month = 10, day =   8},
        [5] = date{year = 2024, month = 12, day =   7},
    },
}

local img = cp.new(1200, 480)
local start_date = 2004

local xTickLabels = {}
local yTickLabels = {}
for y, minor, set in sorted_pairs(data) do
    local lbl = '1.' .. tostring(minor)
    yTickLabels[y]= string.format('%-4s', lbl)
    local xdata = {}
    local ydata = {}
    local tags = {}
    local max_patch
    for _, patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - start_date] = tostring(year)
        xdata[#xdata+1] = date - start_date
        ydata[#ydata+1] = y
        tags[#tags+1] = false
        max_patch = patch
    end
    tags[#tags] = lbl .. '.' .. tostring(max_patch)
    img:setData(xdata, ydata, 'blue')
    img:setTag(tags)
end

img:setGraphOptions{
    title               = 'Releases of Subversion',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    vertGraphOffset     = 15,
}

local f = assert(io.open('svn.png', 'wb'))
f:write(img:draw())
f:close()
