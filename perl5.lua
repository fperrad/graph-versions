#!/usr/bin/env lua

local http = require'ssl.https'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return i, k, t[k]
        end
    end
end

local months = {
    Jan = 1,
    Feb = 2,
    Mar = 3,
    Apr = 4,
    May = 5,
    Jun = 6,
    Jul = 7,
    Aug = 8,
    Sep = 9,
    Oct = 10,
    Nov = 11,
    Dec = 12,
}

local content = assert(http.request'https://raw.githubusercontent.com/Perl/perl5/blead/pod/perlhist.pod')
local data = {}
for line in content:gmatch'([^\n]*)\n?' do
    if line:match'^=head2 SELECTED' then
        break
    end
    local minor, patch, year, month, day = line:match'5%.(%d+)%.(%d+)%s+(%d%d%d%d)%-(%w%w%w)%-(%d%d)'
    minor = tonumber(minor)
    if minor and (minor % 2) == 0 then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][tonumber(patch)] = date{year = tonumber(year), month = months[month], day = tonumber(day)}
    end
end

local img = cp.new(1200, 900)
local start_date = 2000

local xTickLabels = {}
local yTickLabels = {}
for y, minor, set in sorted_pairs(data) do
    local lbl = '5.' .. tostring(minor)
    yTickLabels[y]= string.format('%-4s', lbl)
    local xdata = {}
    local ydata = {}
    local tags = {}
    local prev_date = start_date
    for _, patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - start_date] = tostring(year)
        xdata[#xdata+1] = date - start_date
        ydata[#ydata+1] = y
        if date - prev_date < 0.03 then
            tags[#tags] = false
        end
        lbl = '5.' .. tostring(minor) .. '.' .. tostring(patch)
        tags[#tags+1] = lbl
        prev_date = date
    end
    if minor <= 8 then
        local xdata2 = { xdata[#xdata-1], xdata[#xdata] }
        local ydata2 = { y, y }
        local tags2 = { false, tags[#tags] }
        img:setData(xdata2, ydata2, 'blue dashedline up')
        img:setTag(tags2)
        xdata[#xdata] = nil
        ydata[#ydata] = nil
        tags[#tags] = nil
    end
    img:setData(xdata, ydata, 'blue up')
    img:setTag(tags)
end

img:setGraphOptions{
    title               = 'Releases of Perl 5',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    horGraphOffset      = 40,
    vertGraphOffset     = 15,
}

local f = assert(io.open('perl5.png', 'wb'))
f:write(img:draw())
f:close()
