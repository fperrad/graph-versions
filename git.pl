#!/usr/bin/env perl

use strict;
use warnings;

use DateTime;
use Chart::Plot::Tagged;

if (-d 'git') {
    system 'cd git && git pull';
}
else {
    system 'git clone git://github.com/git/git.git';
}

chdir 'git';
open (my $fh, '-|', 'git log --tags --pretty=format:"%ai %d"') or die;
my $git = {};
while (<$fh>) {
    if (/^(\d\d\d\d)-(\d\d)-(\d\d) \d\d:\d\d:\d\d [^ ]+\s+\(([^)]+)\)/) {
        my $date = DateTime->new(
            year => $1,
            month => $2,
            day => $3,
        );
        for my $tag (split ", ", $4) {
            if ($tag =~ /^(tag: )?v([1-9])\.(\d+)\.(\d+)(\.(\d+))?$/) {
                if ($2 == 1 && $3 >= 4 && $3 <= 8) {
                    $git->{$2 . '.' . $3 . '.' . sprintf("%02d", $4)}->{$6||'0'} = $date;
                }
                elsif ($2 == 2) {
                    $git->{$2 . '.' . sprintf("%02d", $3)}->{$4} = $date;
                }
                else {
                    $git->{$2 . '.' . $3}->{$4} = $date;
                }
            }
        }
    }
}
close $fh;
chdir '..';


my $img = Chart::Plot::Tagged->new(1200, 600);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort keys %{$git}) {
    my $lbl = $major;
    $y++ if $lbl eq '1.9';
    $y++ if $lbl !~ /^2/ && $lbl =~ /\.00$/;
    $lbl .= '  ' if length $lbl < 5;
    $lbl =~ s/\.0(\d)/\.$1/;
    $lbl .= ' ' if length $lbl < 6;
    $yTickLabels{$y} = $lbl;
    my $set = $git->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2005} = '' . $date->year;
        push @xdata, $date->year - 2005 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    $lbl = $major;
    $lbl .= '.' . $max if $max > 0 || $major gt '1.9';
    $lbl =~ s/\.0(\d)/\.$1/;
    push @tags, $lbl;
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of Git',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open $fh, '>', 'git.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

