#!/usr/bin/env lua

local http = require'ssl.https'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return i, k, t[k]
        end
    end
end

local typescript = {
    [2.0]  = '1.8',
    [4.0]  = '2.1',
    [5.0]  = '2.4',
    [5.1]  = '2.5',
    [6.0]  = '2.7',
    [6.1]  = '2.9',
    [7.0]  = '3.1',
    [7.2]  = '3.2',
    [8.0]  = '3.4',
    [8.2]  = '3.5',
    [9.0]  = '3.7',
    [9.1]  = '3.8',
    [10.0] = '3.9',
    [11.0] = '4.0',
    [11.1] = '4.1',
    [12.0] = '4.2',
    [12.1] = '4.3',
    [13.0] = '4.4',
    [13.1] = '4.5',
    [13.3] = '4.6',
}

local content = assert(http.request'https://raw.githubusercontent.com/angular/angular/master/CHANGELOG.md')
local data = {}
for line in content:gmatch'([^\n]*)\n?' do
    local minor, patch, rc, year, month, day = line:match'# %[(%d+%.%d+)%.(%d+)([^%]]*)%]%([^%)]+%)[%s%w%-]+%((%d+)%-(%d+)%-(%d+)%)'
    if day and rc:sub(1, 1) ~= '-' then
        minor = tonumber(minor)
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][tonumber(patch)] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
    minor, patch, year, month, day = line:match'# (%d+%.%d+)%.(%d+)%s+%((%d+)%-(%d+)%-(%d+)%)'
    if day then
        minor = tonumber(minor)
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][tonumber(patch)] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
end

local img = cp.new(1200, 480)
local start_date = 2016

local xTickLabels = {}
local yTickLabels = {}
local xtypescript = {}
local ytypescript = {}
local ttypescript = {}
for y, minor, set in sorted_pairs(data) do
    if typescript[minor] then
        local _, _, date = sorted_pairs(set)()
        xtypescript[#xtypescript+1] = date - start_date
        ytypescript[#ytypescript+1] = y
        ttypescript[#ttypescript+1] = 'TS ' .. typescript[minor]
    end
    yTickLabels[y]= string.format('%.1f', minor)
    local xdata = {}
    local ydata = {}
    local tags = {}
    local max_patch
    for _, patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - start_date] = tostring(year)
        xdata[#xdata+1] = date - start_date
        ydata[#ydata+1] = y
        tags[#tags+1] = false
        max_patch = patch
    end
    local lbl = string.format('%.1f', minor) .. '.' .. max_patch
    tags[#tags] = lbl
    img:setData(xdata, ydata, 'blue')
    img:setTag(tags)
end
img:setData(xtypescript, ytypescript, 'nopoints nolines blue up')
img:setTag(ttypescript)

img:setGraphOptions{
    title               = 'Releases of Angular',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    vertGraphOffset     = 15,
}

local f = assert(io.open('angular2.png', 'wb'))
f:write(img:draw())
f:close()
