#!/usr/bin/env lua

local lfs = require'lfs'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return k, t[k]
        end
    end
end

if not lfs.attributes'git' then
    os.execute'git clone https://github.com/git/git.git'
else
    os.execute'cd git && git pull --no-rebase'
end

local data = {}
for line in io.popen'cd git && git log --tags --pretty=format:"%ai %d"':lines() do
    local year, month, day, tags = line:match'^(%d%d%d%d)%-(%d%d)%-(%d%d) %d%d:%d%d:%d%d [^ ]+%s+%(([^)]+)%)'
    if tags then
        for tag in tags:gmatch'([^,]*),?' do
            local v1, v2, v3, v4 = tag:match'tag: v([1-9])%.(%d+)%.(%d+)%.(%d+)$'
            if v1 then
                local minor = v1 .. '.' .. v2 .. '.' .. string.format('%02d', tonumber(v3))
                if not data[minor] then
                    data[minor] = {}
                end
                data[minor][tonumber(v4)] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
            end
            v1, v2, v3 = tag:match'tag: v([1-9])%.(%d+)%.(%d+)$'
            if v1 then
                local minor
                local patch = v3
                if v1 == '1' and v2 >= '4' and v2 <= '8' then
                    minor = v1 .. '.' .. v2 .. '.' .. string.format('%02d', tonumber(v3))
                    patch = -1
                elseif v1 == '2' then
                    minor = v1 .. '.' .. string.format('%02d', tonumber(v2))
                else
                    minor = v1 .. '.' .. v2
                end
                if not data[minor] then
                    data[minor] = {}
                end
                data[minor][tonumber(patch)] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
            end
        end
    end
end

local img = cp.new(1200, 800)
local start_date = 2005

local xTickLabels = {}
local yTickLabels = {}
local y = 1
for minor, set in sorted_pairs(data) do
    local lbl = minor
    if lbl == '1.9' then
        y = y + 1
    end
    if not lbl:match'^2' and lbl:match'%.00$' then
        y = y + 1
    end
    lbl = lbl:gsub('%.0(%d)', '.%1')
    yTickLabels[y]= string.format('%-6s', lbl)
    local xdata = {}
    local ydata = {}
    local tags = {}
    local max_patch
    for patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - start_date] = tostring(year)
        xdata[#xdata+1] = date - start_date
        ydata[#ydata+1] = y
        tags[#tags+1] = false
        max_patch = patch
    end
    if max_patch >= 0 then
        lbl = lbl .. '.' .. tostring(max_patch)
    end
    tags[#tags] = lbl
    img:setData(xdata, ydata, 'blue')
    img:setTag(tags)
    y = y + 1
end

img:setGraphOptions{
    title               = 'Releases of Git',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    vertGraphOffset     = 15,
}

local f = assert(io.open('git.png', 'wb'))
f:write(img:draw())
f:close()
