#!/usr/bin/env lua

local lfs = require'lfs'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return k, t[k]
        end
    end
end

if not lfs.attributes'gitlab-foss' then
    os.execute'git clone https://gitlab.com/gitlab-org/gitlab-foss.git'
else
    os.execute'cd gitlab-foss && git pull --no-rebase'
end

local data = {}
for line in io.popen'cd gitlab-foss && git log --tags --pretty=format:"%ai %d"':lines() do
    local year, month, day, tags = line:match'^(%d%d%d%d)%-(%d%d)%-(%d%d) %d%d:%d%d:%d%d [^ ]+%s+%(([^)]+)%)'
    if tags then
        for tag in tags:gmatch'([^,]*),?' do
            local v1, v2, v3 = tag:match'tag: v(%d+)%.(%d+)%.(%d+)$'
            v1 = tonumber(v1)
            if v1 and v1 >= 9 then
                local minor = string.format('%02d.%02d', v1, tonumber(v2))
                if not data[minor] then
                    data[minor] = {}
                end
                data[minor][tonumber(v3)] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
            end
        end
    end
end

local img = cp.new(1200, 960)
local start_date = 2017

local xTickLabels = {}
local yTickLabels = {}
local y = 1
for minor, set in sorted_pairs(data) do
    local lbl = minor
    if lbl:match'%.00$' then
        y = y + 1
    end
    lbl = lbl:gsub('^0', ' ')
    lbl = lbl:gsub('%.0(%d)', '.%1')
    yTickLabels[y]= string.format('%-5s', lbl)
    local xdata = {}
    local ydata = {}
    local tags = {}
    local max_patch
    for patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - start_date] = tostring(year)
        xdata[#xdata+1] = date - start_date
        ydata[#ydata+1] = y
        tags[#tags+1] = false
        max_patch = patch
    end
    lbl = lbl .. '.' .. tostring(max_patch)
    tags[#tags] = lbl
    img:setData(xdata, ydata, 'blue')
    img:setTag(tags)
    y = y + 1
end

img:setGraphOptions{
    title               = 'Releases of Gitlab',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    vertGraphOffset     = 15,
}

local f = assert(io.open('gitlab.png', 'wb'))
f:write(img:draw())
f:close()
