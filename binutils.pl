#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);
my $response = $ua->get('http://ftp.gnu.org/gnu/binutils/');
die $response->status_line unless $response->is_success;

my $binutils = {};
for (split "\n", $response->decoded_content) {
    if (/>binutils-2\.(\d+)\.?(\d+)?\.tar\.gz<[^>]+><[^>]+><[^>]+>(\d{4})-(\d{2})-(\d{2})/) {
        next if $1 < 10;
        my $date = DateTime->new(
            year => $3,
            month => $4,
            day => $5,
        );
        $binutils->{$1}->{$2||'0'} = $date;
    }
}
my $unk = 15;
$binutils->{'15'}->{'0'} = DateTime->new(year => 2004, month =>  5, day => $unk);
$binutils->{'16'}->{'0'} = DateTime->new(year => 2005, month =>  5, day => $unk);
$binutils->{'16'}->{'1'} = DateTime->new(year => 2005, month =>  6, day => $unk);
$binutils->{'17'}->{'0'} = DateTime->new(year => 2006, month =>  6, day => $unk);
$binutils->{'18'}->{'0'} = DateTime->new(year => 2007, month =>  8, day => $unk);
$binutils->{'19'}->{'0'} = DateTime->new(year => 2008, month => 10, day => 27);
$binutils->{'19'}->{'1'} = DateTime->new(year => 2009, month =>  2, day =>  2);
$binutils->{'20'}->{'0'} = DateTime->new(year => 2009, month => 10, day => 19);
$binutils->{'20'}->{'1'} = DateTime->new(year => 2010, month =>  3, day =>  8);
$binutils->{'21'}->{'0'} = DateTime->new(year => 2010, month => 12, day =>  9);
$binutils->{'21'}->{'1'} = DateTime->new(year => 2011, month =>  6, day => 27);

my $img = Chart::Plot::Tagged->new(1200, 480);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$binutils}) {
    my $lbl = '2.' . $major;
    $yTickLabels{$y} = $lbl;
    my $set = $binutils->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2000} = '' . $date->year;
        push @xdata, $date->year - 2000 + $date->day_of_year/365;
        push @ydata, $y;
        $lbl = '2.' . $major;
        $lbl .= '.' . $minor if $minor > 0;
        push @tags, $lbl;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, undef;
    $img->setData(\@xdata, \@ydata, 'blue up');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of binutils',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'binutils.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

