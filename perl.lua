#!/usr/bin/env lua

local http = require'ssl.https'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return k, t[k]
        end
    end
end

local months = {
    Jan = 1,
    Feb = 2,
    Mar = 3,
    Apr = 4,
    May = 5,
    Jun = 6,
    Jul = 7,
    Aug = 8,
    Sep = 9,
    Oct = 10,
    Nov = 11,
    Dec = 12,
}

local content = assert(http.request'https://raw.githubusercontent.com/Perl/perl5/blead/pod/perlhist.pod')
local data = {}
for line in content:gmatch'([^\n]*)\n?' do
    if line:match'^=head2 SELECTED' then
        break
    end
    local minor, patch, year, month, day
    minor, patch, year, month, day = line:match'([1-4])%.(0%d%d)%s+(%d%d%d%d)%-(%w%w%w)%-(%d%d)'
    minor = tonumber(minor)
    if minor then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][patch] = date{year = tonumber(year), month = months[month], day = tonumber(day)}
    end
    minor, patch, year, month, day = line:match'([1-4])%.0%d%d%.%.(%d+)%s+(%d%d%d%d)%-(%w%w%w)%-(%d%d)'
    minor = tonumber(minor)
    if minor then
        if not data[minor] then
            data[minor] = {}
        end
        patch = string.format('%03d', tonumber(patch))
        data[minor][patch] = date{year = tonumber(year), month = months[month], day = tonumber(day)}
    end
    minor, year, month, day = line:match'(5%.00%d)%s+(%d%d%d%d)%-(%w%w%w)%-(%d%d)'
    minor = tonumber(minor)
    if minor then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][' '] = date{year = tonumber(year), month = months[month], day = tonumber(day)}
    end
    minor, patch, year, month, day = line:match'(5%.00%d)(%l)%s+(%d%d%d%d)%-(%w%w%w)%-(%d%d)'
    minor = tonumber(minor)
    if minor then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][patch] = date{year = tonumber(year), month = months[month], day = tonumber(day)}
    end
    minor, patch, year, month, day = line:match'(5%.00%d)(_[0-2]%d+)%s+(%d%d%d%d)%-(%w%w%w)%-(%d%d)'
    minor = tonumber(minor)
    if minor then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][patch] = date{year = tonumber(year), month = months[month], day = tonumber(day)}
    end
    minor, patch, year, month, day = line:match'5%.(%d+)%.(%d+)%s+(%d%d%d%d)%-(%w%w%w)%-(%d%d)'
    minor = tonumber(minor)
    if minor and minor >= 6 and (minor % 2) == 0 then
        minor = tonumber(string.format('5.%02d', minor))
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][tonumber(patch)] = date{year = tonumber(year), month = months[month], day = tonumber(day)}
    end
end

local img = cp.new(1200, 480)
local start_date = 1987

local xTickLabels = {}
local yTickLabels = {}
local y = 1
for minor, set in sorted_pairs(data) do
    local lbl
    if minor == 5.000 then
        lbl = '5.000'
    elseif minor >= 5.06 then
        lbl = string.format('%.2f', minor):gsub('%.0([68])', '.%1')
    else
        lbl = tostring(minor)
    end
    yTickLabels[y]= string.format('%-5s', lbl)
    local xdata = {}
    local ydata = {}
    local tags = {}
    local max_patch
    for patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - start_date] = tostring(year)
        xdata[#xdata+1] = date - start_date
        ydata[#ydata+1] = y
        tags[#tags+1] = false
        max_patch = tostring(patch)
    end
    if max_patch:match'^[_%l]' then
        tags[#tags] = lbl .. tostring(max_patch)
    else
        tags[#tags] = lbl .. '.' .. tostring(max_patch)
    end
    if minor >= 5.004 and minor <= 5.08 then
        local xdata2 = { xdata[#xdata-1], xdata[#xdata] }
        local ydata2 = { y, y }
        local tags2 = { false, tags[#tags] }
        img:setData(xdata2, ydata2, 'blue dashedline')
        img:setTag(tags2)
        xdata[#xdata] = nil
        ydata[#ydata] = nil
        tags[#tags] = nil
    end
    img:setData(xdata, ydata, 'blue')
    img:setTag(tags)
    y = y + 1
    if minor < 5 or (minor >= 5.005 and minor < 5.12) then
        y = y + 1
    end
end

img:setGraphOptions{
    title               = 'Releases of Perl',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    horGraphOffset      = 50,
    vertGraphOffset     = 15,
}

local f = assert(io.open('perl.png', 'wb'))
f:write(img:draw())
f:close()
