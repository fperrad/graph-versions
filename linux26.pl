#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use GD::Graph::bars;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);
my $response = $ua->get('https://www.kernel.org/pub/linux/kernel/v2.6/');
die $response->status_line unless $response->is_success;

my %month = (
    Jan => 1,
    Feb => 2,
    Mar => 3,
    Apr => 4,
    May => 5,
    Jun => 6,
    Jul => 7,
    Aug => 8,
    Sep => 9,
    Oct => 10,
    Nov => 11,
    Dec => 12,
);

my $kernel = {};
for (split "\n", $response->decoded_content) {
    if (/>linux-2\.6\.(\d+)\.?(\d+)?\.tar\.gz<\/a>\s+(\d{2})-(\w{3})-(\d{4})\s+\d{2}:\d{2}\s+(\d+)/) {
        my $size = $6;
        my $date = DateTime->new(
            year => $5,
            month => $month{$4},
            day => $3,
        );
        $kernel->{$1}->{$2||'0'} = [ $size, $date ];
    }
}

$response = $ua->get('https://www.kernel.org/pub/linux/kernel/v2.6/longterm/');
die $response->status_line unless $response->is_success;

my @urls = ();
for (split "\n", $response->decoded_content) {
    if (/>(v2\.6\.\d+)\/</) {
        push @urls, 'https://www.kernel.org/pub/linux/kernel/v2.6/longterm/' . $1 . '/';
    }
}

my $ltkernel = {};
for (@urls) {
    $response = $ua->get($_);
    die $response->status_line unless $response->is_success;

    for (split "\n", $response->decoded_content) {
        if (/>linux-2\.6\.(\d+)\.?(\d+)?\.tar\.gz<\/a>\s+(\d{2})-(\w{3})-(\d{4})\s+\d{2}:\d{2}\s+(\d+)/) {
            my $size = $6;
            my $date = DateTime->new(
                year => $5,
                month => $month{$4},
                day => $3,
            );
            $ltkernel->{$1}->{$2||'0'} = [ $size, $date ];
        }
    }
}

$response = $ua->get('https://www.kernel.org/pub/linux/kernel/v3.x/');
die $response->status_line unless $response->is_success;
my $kernel3 = {};
for (split "\n", $response->decoded_content) {
    if (/>linux-3\.(\d+)\.?(\d+)?\.tar\.gz<\/a>\s+(\d{2})-(\w{3})-(\d{4})\s+\d{2}:\d{2}\s+(\d+)/) {
        my $size = $6;
        my $date = DateTime->new(
            year => $5,
            month => $month{$4},
            day => $3,
        );
        $kernel3->{$1}->{$2||'0'} = [ $size, $date ];
    }
}

my @majors;
my @sizes;
for my $major (sort {$a <=> $b} keys %{$kernel}) {
    push @majors, $major;
    my $data = $kernel->{$major}->{'0'};
    my $size = $data->[0];
    push @sizes, $size / 1024 / 1024;
}
for my $major (sort {$a <=> $b} keys %{$kernel3}) {
    push @majors, $major;
    my $data = $kernel3->{$major}->{'0'};
    my $size = $data->[0];
    push @sizes, $size / 1024 / 1024;
}

my $graph = GD::Graph::bars->new(640, 480);
$graph->set(
    title               => 'Size of Linux 2.6 & 3 (src.tar.gz)',
    y_label             => 'Mo',
    y_min_values        => 0,
    y_max_values        => 80,
    y_tick_number       => 8,
);
my $gd = $graph->plot( [ \@majors, \@sizes ] );

open my $fh, '>', 'size26.png' or die $!;
binmode $fh;
print {$fh} $gd->png;
close $fh;

my $img = Chart::Plot::Tagged->new(1200, 560);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$kernel}) {
    my $lbl = '2.6.' . $major;
    $lbl .= ' ' if length $lbl < 6;
    $yTickLabels{$y} = $lbl;
    my $set = $kernel->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $data = $set->{$minor};
        my $date = $data->[1];
        $xTickLabels{'' . $date->year - 2003} = '' . $date->year;
        push @xdata, $date->year - 2003 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, undef;
    if (exists $ltkernel->{$major}) {
        my $set = $ltkernel->{$major};
        my @ltxdata = ($xdata[-1]);
        my @ltydata = ($ydata[-1]);
        my @lttags = (undef);
        my $max;
        for my $minor (sort {$a <=> $b} keys %{$set}) {
            my $data = $set->{$minor};
            my $date = $data->[1];
            $xTickLabels{'' . $date->year - 2003} = '' . $date->year;
            push @ltxdata, $date->year - 2003 + $date->day_of_year/365;
            push @ltydata, $y;
            push @lttags, undef;
            $max = $minor;
        }
        pop @lttags;
        $lbl = '2.6.' . $major;
        $lbl .= '.' . $max if $max > 0;
        push @lttags, $lbl;
        $img->setData(\@ltxdata, \@ltydata, 'blue dashedline');
        $img->setTag(\@lttags);
    }
    else {
        pop @tags;
        $lbl = '2.6.' . $major;
        $lbl .= '.' . $max if $max > 0;
        push @tags, $lbl;
    }
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
}

for my $major (sort {$a <=> $b} keys %{$kernel3}) {
    my $lbl = '3.' . $major . '  ';
    $lbl .= ' ' if length $lbl < 6;
    $yTickLabels{$y} = $lbl;
    my $set = $kernel3->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $data = $set->{$minor};
        my $date = $data->[1];
        $xTickLabels{'' . $date->year - 2003} = '' . $date->year;
        push @xdata, $date->year - 2003 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, undef;
    #~ if (exists $ltkernel3->{$major}) {
        #~ my $set = $ltkernel3->{$major};
        #~ my @ltxdata = ($xdata[-1]);
        #~ my @ltydata = ($ydata[-1]);
        #~ my @lttags = (undef);
        #~ my $max;
        #~ for my $minor (sort {$a <=> $b} keys %{$set}) {
            #~ my $data = $set->{$minor};
            #~ my $date = $data->[1];
            #~ $xTickLabels{'' . $date->year - 2003} = '' . $date->year;
            #~ push @ltxdata, $date->year - 2003 + $date->day_of_year/365;
            #~ push @ltydata, $y;
            #~ push @lttags, undef;
            #~ $max = $minor;
        #~ }
        #~ pop @lttags;
        #~ $lbl = '2.6.' . $major;
        #~ $lbl .= '.' . $max if $max > 0;
        #~ push @lttags, $lbl;
        #~ $img->setData(\@ltxdata, \@ltydata, 'blue dashedline');
        #~ $img->setTag(\@lttags);
    #~ }
    #~ else {
        pop @tags;
        $lbl = '3.' . $major;
        $lbl .= '.' . $max if $max > 0;
        push @tags, $lbl;
    #~ }
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of Linux 2.6 & 3',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    horGraphOffset      => 75,
    vertGraphOffset     => 15,
);

open $fh, '>', 'version26.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

