#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);
my $response = $ua->get('http://crosstool-ng.org/download/crosstool-ng/');
die $response->status_line unless $response->is_success;

my %month = (
    Jan => 1,
    Feb => 2,
    Mar => 3,
    Apr => 4,
    May => 5,
    Jun => 6,
    Jul => 7,
    Aug => 8,
    Sep => 9,
    Oct => 10,
    Nov => 11,
    Dec => 12,
);

my $crosstool = {};
for (split 'href', $response->decoded_content) {
    if (/>crosstool-ng-1\.(\d+)\.(\d+)\.tar\.bz2<[^>]+><[^>]+><[^>]+>(\d{4})-(\w{3})-(\d{2})/) {
        my $date = DateTime->new(
            year => $3,
            month => $month{$4},
            day => $5,
        );
        $crosstool->{$1}->{$2} = $date;
    }
}

my $img = Chart::Plot::Tagged->new(1200, 480);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$crosstool}) {
    my $lbl = '1.' . $major;
    $yTickLabels{$y} = $lbl;
    my $set = $crosstool->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2008} = '' . $date->year;
        push @xdata, $date->year - 2008 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, '1.' . $major . '.' . $max;
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of crosstool-NG',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'crosstool.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

