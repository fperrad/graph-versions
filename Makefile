
.PHONY: all
all: \
  lua.png \
  perl5.png \
  perl.png \
  gcc.png \
  glibc.png \
  binutils.png \
  gmp.png \
  busybox.png \
  uClibc.png \
  musl.png \
  crosstool.png \
  u-boot.png \
  svn.png \
  trac.png \
  git.png \
  gitlab.png \
  linux.png \
  webkitgtk.png \
  wpewebkit.png \
  webpack.png \
  typescript.png \
  assemblyscript.png \
  cypress.png \
  nodejs.png \
  deno.png \
  eslint.png \
  jest.png \
  mocha.png \
  playwright.png \
  prettier.png \
  svelte.png \
  rollup.png \
  testcafe.png \
  vite.png \
  vitest.png

.PHONY: pages
pages:
	mkdir public
	cp *.html       public/
	cp *.png        public/

update_pl = \
use strict; \
use warnings; \
if (qx{git status} !~ m{nothing( added)? to commit}) { \
    print qq{commit needed\n}; \
    qx{git commit -a -m "update"}; \
    qx{git push}; \
    print qq{pushed\n}; \
}

.PHONY: update
update: clean all
	perl -e '$(update_pl)'

linux.png:
	./linux.lua

lua.png:
	./lua.lua

perl5.png:
	./perl5.lua

perl.png:
	./perl.lua

gcc.png:
	./gcc.lua

glibc.png:
	./glibc.lua

binutils.png:
	./binutils.lua

gmp.png:
	./gmp.lua

busybox.png:
	./busybox.lua

uClibc.png:
	./uClibc.lua

musl.png:
	./musl.lua

crosstool.png:
	./crosstool.lua

u-boot.png:
	./u-boot.lua

svn.png:
	./svn.lua

trac.png:
	./trac.lua

git.png:
	./git.lua

gitlab.png:
	./gitlab.lua

webkitgtk.png:
	./webkitgtk.lua

wpewebkit.png:
	./wpewebkit.lua

webpack.png:
	./webpack.lua

typescript.png:
	./typescript.lua

nodejs.png:
	./nodejs.lua

assemblyscript.png:
	./assemblyscript.lua

cypress.png:
	./cypress.lua

deno.png:
	./deno.lua

eslint.png:
	./eslint.lua

jest.png:
	./jest.lua

mocha.png:
	./mocha.lua

prettier.png:
	./prettier.lua

playwright.png:
	./playwright.lua

svelte.png:
	./svelte.lua

rollup.png:
	./rollup.lua

testcafe.png:
	./testcafe.lua

vite.png:
	./vite.lua

vitest.png:
	./vitest.lua

.PHONY: check
check:
	xmllint --noout --valid index.html
	xmllint --noout --valid embedded.html
	xmllint --noout --valid interp.html
	xmllint --noout --valid linux.html
	xmllint --noout --valid perl.html
	xmllint --noout --valid tools.html
	xmllint --noout --valid scm.html
	xmllint --noout --valid webkit.html
	xmllint --noout --valid svelte.html

.PHONY: clean
clean:
	-rm *.png
