#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);
my $response = $ua->get('https://www.busybox.net/downloads/');
die $response->status_line unless $response->is_success;

my $busybox = {};
for (split "\n", $response->decoded_content) {
    if (/>busybox-1\.(\d+)\.(\d+)\.tar\.bz2<\/a>\s+(\d{4})-(\d{2})-(\d{2})/) {
        my $date = DateTime->new(
            year => $3,
            month => $4,
            day => $5,
        );
        $busybox->{$1}->{$2} = $date;
    }
}

my $img = Chart::Plot::Tagged->new(1200, 480);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$busybox}) {
    my $lbl = '1.' . $major;
    $yTickLabels{$y} = $lbl;
    my $set = $busybox->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2006} = '' . $date->year;
        push @xdata, $date->year - 2006 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, '1.' . $major . '.' . $max;
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of busybox',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'busybox.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

