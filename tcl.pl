#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);
my $response = $ua->get('ftp://mirror.switch.ch/mirror/tcl.tk/');
die $response->status_line unless $response->is_success;

my @urls = ();
for (split "\n", $response->decoded_content) {
    my @entry = split /\s+/;
    next unless $entry[8] =~ /tcl8_\d/;
    push @urls, 'ftp://mirror.switch.ch/mirror/tcl.tk/' . $entry[8] . '/';
}

my %month = (
    Jan => 1,
    Feb => 2,
    Mar => 3,
    Apr => 4,
    May => 5,
    Jun => 6,
    Jul => 7,
    Aug => 8,
    Sep => 9,
    Oct => 10,
    Nov => 11,
    Dec => 12,
);

my $tcl = {};
for (@urls) {
    $response = $ua->get($_);
    die $response->status_line unless $response->is_success;

    for (split "\n", $response->decoded_content) {
        my @entry = split /\s+/;
        my $filename = $entry[8];
        if ($filename =~ /^tcl8\.(\d+)\.(\d+)(-src)?\.tar\.gz$/) {
            my $size = $entry[4];
            my $month = $entry[5];
            my $day = $entry[6];
            my $year = $entry[7];
            if (index($year, ':') > 0) {
                my @now = gmtime(time);
                $year = 1900 + $now[5];
                $year-- if $month{$month} > $now[4]+1;
            }
            my $date = DateTime->new(
                year => $year,
                month => $month{$month},
                day => $day,
            );
            $tcl->{$1}->{$2} = [ $size, $date ];
        }
    }
}

my $img = Chart::Plot::Tagged->new(1200, 480);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$tcl}) {
    my $lbl = '8.' . $major;
    $yTickLabels{$y} = $lbl;
    my $set = $tcl->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $data = $set->{$minor};
        my $date = $data->[1];
        $xTickLabels{'' . $date->year - 2000} = '' . $date->year;
        push @xdata, $date->year - 2000 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, '8.' . $major . '.' . $minor;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, undef;
    $img->setData(\@xdata, \@ydata, 'up blue');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of Tcl',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'tcl.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

