#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);
my $response = $ua->get('http://ftp.gnu.org/gnu/gmp/');
die $response->status_line unless $response->is_success;

my $gmp = {};
for (split "\n", $response->decoded_content) {
    if (/>gmp-(4|5|6)\.(\d+)\.?(\d+)?a?\.tar\.[gx]z<[^>]+><[^>]+><[^>]+>(\d{4})-(\d{2})-(\d{2})/) {
        my $date = DateTime->new(
            year => $4,
            month => $5,
            day => $6,
        );
        $gmp->{$1}->{$2}->{$3||'0'} = $date;
    }
}


my $img = Chart::Plot::Tagged->new(1200, 480);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $version (sort {$a <=> $b} keys %{$gmp}) {
    for my $major (sort {$a <=> $b} keys %{$gmp->{$version}}) {
        my $lbl = $version . '.' . $major;
        $yTickLabels{$y} = $lbl;
        my $set = $gmp->{$version}->{$major};
        my @xdata;
        my @ydata;
        my @tags;
        for my $minor (sort {$a <=> $b} keys %{$set}) {
            my $date = $set->{$minor};
            $xTickLabels{'' . $date->year - 2001} = '' . $date->year;
            push @xdata, $date->year - 2001 + $date->day_of_year/365;
            push @ydata, $y;
            $lbl = $version . '.' . $major;
            $lbl .= '.' . $minor if $minor > 0 || $major >= 3 || $version >= 5;
            push @tags, $lbl;
        }
        push @xdata, $xdata[-1] + 0.001;
        push @ydata, $ydata[-1];
        push @tags, undef;
        $img->setData(\@xdata, \@ydata, 'blue up');
        $img->setTag(\@tags);
        $y++;
    }
}

$img->setGraphOptions(
    title               => 'Releases of gmp',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'gmp.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

