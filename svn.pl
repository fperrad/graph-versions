#!/usr/bin/env perl

use strict;
use warnings;

use DateTime;
use Chart::Plot::Tagged;

# see http://svn.apache.org/repos/asf/subversion/trunk/CHANGES
my $svn = {
    '0' => {
        '0' => DateTime->new(year => 2004, month =>  2, day =>  23),
        '1' => DateTime->new(year => 2004, month =>  3, day =>  12),
        '2' => DateTime->new(year => 2004, month =>  4, day =>  15),
        '3' => DateTime->new(year => 2004, month =>  5, day =>  19),
        '4' => DateTime->new(year => 2004, month =>  5, day =>  21),
        '5' => DateTime->new(year => 2004, month =>  6, day =>  10),
        '6' => DateTime->new(year => 2004, month =>  7, day =>  19),
        '7' => DateTime->new(year => 2004, month =>  9, day =>  17),
        '8' => DateTime->new(year => 2004, month =>  9, day =>  22),
        '9' => DateTime->new(year => 2004, month => 10, day =>  13),
    },
    '1' => {
        '0' => DateTime->new(year => 2004, month =>  9, day =>  29),
        '1' => DateTime->new(year => 2004, month => 10, day =>  22),
        '2' => DateTime->new(year => 2004, month => 12, day =>  20),
        '3' => DateTime->new(year => 2005, month =>  1, day =>  14),
        '4' => DateTime->new(year => 2005, month =>  4, day =>   1),
    },
    '2' => {
        '0' => DateTime->new(year => 2005, month =>  5, day =>  21),
        '1' => DateTime->new(year => 2005, month =>  7, day =>   5),
        '3' => DateTime->new(year => 2005, month =>  8, day =>  19),
    },
    '3' => {
        '0' => DateTime->new(year => 2005, month => 12, day =>  30),
        '1' => DateTime->new(year => 2006, month =>  3, day =>  25),
        '2' => DateTime->new(year => 2006, month =>  5, day =>  23),
    },
    '4' => {
        '0' => DateTime->new(year => 2006, month =>  9, day =>  10),
        '2' => DateTime->new(year => 2006, month => 11, day =>   2),
        '3' => DateTime->new(year => 2007, month =>  1, day =>  18),
        '4' => DateTime->new(year => 2007, month =>  5, day =>  30),
        '5' => DateTime->new(year => 2007, month =>  8, day =>  27),
        '6' => DateTime->new(year => 2007, month => 12, day =>  21),
    },
    '5' => {
        '0' => DateTime->new(year => 2008, month =>  6, day =>  19),
        '1' => DateTime->new(year => 2008, month =>  7, day =>  26),
        '2' => DateTime->new(year => 2008, month =>  8, day =>  30),
        '3' => DateTime->new(year => 2008, month => 10, day =>  10),
        '4' => DateTime->new(year => 2008, month => 10, day =>  24),
        '5' => DateTime->new(year => 2008, month => 12, day =>  22),
        '6' => DateTime->new(year => 2009, month =>  2, day =>  26),
        '7' => DateTime->new(year => 2009, month =>  8, day =>   6),
        '9' => DateTime->new(year => 2010, month => 12, day =>   6),
    },
    '6' => {
        '0' => DateTime->new(year => 2009, month =>  3, day =>  20),
        '1' => DateTime->new(year => 2009, month =>  4, day =>   9),
        '2' => DateTime->new(year => 2009, month =>  5, day =>  11),
        '3' => DateTime->new(year => 2009, month =>  6, day =>  22),
        '4' => DateTime->new(year => 2009, month =>  8, day =>   6),
        '5' => DateTime->new(year => 2009, month =>  8, day =>  22),
        '6' => DateTime->new(year => 2009, month => 10, day =>  22),
        '9' => DateTime->new(year => 2010, month =>  1, day =>  25),
        '11'=> DateTime->new(year => 2010, month =>  4, day =>  19),
        '12'=> DateTime->new(year => 2010, month =>  6, day =>  21),
        '13'=> DateTime->new(year => 2010, month => 10, day =>   1),
        '15'=> DateTime->new(year => 2010, month => 11, day =>  26),
        '16'=> DateTime->new(year => 2011, month =>  3, day =>   2),
        '17'=> DateTime->new(year => 2011, month =>  6, day =>   1),
        '18'=> DateTime->new(year => 2012, month =>  3, day =>  29),
        '19'=> DateTime->new(year => 2012, month =>  9, day =>  10),
        '20'=> DateTime->new(year => 2013, month =>  1, day =>   4),
        '21'=> DateTime->new(year => 2013, month =>  4, day =>   4),
        '23'=> DateTime->new(year => 2013, month =>  5, day =>  30),
    },
    '7' => {
        '0' => DateTime->new(year => 2011, month => 10, day =>  11),
        '1' => DateTime->new(year => 2011, month => 10, day =>  24),
        '2' => DateTime->new(year => 2011, month => 12, day =>   2),
        '3' => DateTime->new(year => 2012, month =>  2, day =>  14),
        '4' => DateTime->new(year => 2012, month =>  3, day =>   8),
        '5' => DateTime->new(year => 2012, month =>  5, day =>  17),
        '6' => DateTime->new(year => 2012, month =>  8, day =>  15),
        '7' => DateTime->new(year => 2012, month => 10, day =>   9),
        '8' => DateTime->new(year => 2012, month => 12, day =>  17),
        '9' => DateTime->new(year => 2013, month =>  4, day =>   4),
        '10'=> DateTime->new(year => 2013, month =>  5, day =>  30),
        '11'=> DateTime->new(year => 2013, month =>  7, day =>  24),
        '13'=> DateTime->new(year => 2013, month =>  8, day =>  30),
        '14'=> DateTime->new(year => 2013, month => 11, day =>  25),
        '16'=> DateTime->new(year => 2014, month =>  2, day =>  26),
        '17'=> DateTime->new(year => 2014, month =>  5, day =>   7),
        '18'=> DateTime->new(year => 2014, month =>  8, day =>  11),
        '19'=> DateTime->new(year => 2014, month => 12, day =>  15),
        '20'=> DateTime->new(year => 2015, month =>  3, day =>  31),
        '21'=> DateTime->new(year => 2015, month =>  8, day =>   5),
        '22'=> DateTime->new(year => 2015, month => 12, day =>  15),
    },
    '8' => {
        '0' => DateTime->new(year => 2013, month =>  6, day =>  18),
        '1' => DateTime->new(year => 2013, month =>  7, day =>  24),
        '3' => DateTime->new(year => 2013, month =>  8, day =>  30),
        '4' => DateTime->new(year => 2013, month => 10, day =>  29),
        '5' => DateTime->new(year => 2013, month => 11, day =>  25),
        '8' => DateTime->new(year => 2014, month =>  2, day =>  19),
        '9' => DateTime->new(year => 2014, month =>  5, day =>   7),
        '10'=> DateTime->new(year => 2014, month =>  8, day =>  11),
        '11'=> DateTime->new(year => 2014, month => 12, day =>  15),
        '13'=> DateTime->new(year => 2015, month =>  3, day =>  31),
        '14'=> DateTime->new(year => 2015, month =>  8, day =>   5),
        '15'=> DateTime->new(year => 2015, month => 12, day =>  15),
        '16'=> DateTime->new(year => 2016, month =>  4, day =>  28),
        '17'=> DateTime->new(year => 2016, month => 11, day =>  29),
        '18'=> DateTime->new(year => 2017, month =>  7, day =>  10),
        '19'=> DateTime->new(year => 2017, month =>  8, day =>  10),
    },
    '9' => {
        '0' => DateTime->new(year => 2015, month =>  8, day =>   5),
        '1' => DateTime->new(year => 2015, month =>  9, day =>   2),
        '2' => DateTime->new(year => 2015, month =>  9, day =>  30),
        '3' => DateTime->new(year => 2015, month => 12, day =>  15),
        '4' => DateTime->new(year => 2016, month =>  4, day =>  28),
        '5' => DateTime->new(year => 2016, month => 11, day =>  29),
        '6' => DateTime->new(year => 2017, month =>  7, day =>   5),
        '7' => DateTime->new(year => 2017, month =>  8, day =>  10),
        '9' => DateTime->new(year => 2018, month =>  7, day =>  20),
        '10'=> DateTime->new(year => 2019, month =>  1, day =>  11),
    },
    '10' => {
        '0' => DateTime->new(year => 2018, month =>  4, day =>  13),
        '2' => DateTime->new(year => 2018, month =>  7, day =>  20),
        '3' => DateTime->new(year => 2018, month => 10, day =>  10),
        '4' => DateTime->new(year => 2019, month =>  1, day =>  11),
    },
    '11' => {
        '0' => DateTime->new(year => 2018, month => 10, day =>  30),
        '1' => DateTime->new(year => 2019, month =>  1, day =>  11),
    },
    '12' => {
        '0' => DateTime->new(year => 2019, month => 10, day =>  12),
    },
};

my $img = Chart::Plot::Tagged->new(1200, 480);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$svn}) {
    my $lbl = '1.' . $major;
    $lbl .= ' ' if length($lbl) < 4;
    $yTickLabels{'' . $y} = $lbl;
    my $set = $svn->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2004} = '' . $date->year;
        push @xdata, $date->year - 2004 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    pop @tags;
    push @tags, '1.' . $major . '.' . $max;
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, undef;
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of Subversion',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'svn.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

