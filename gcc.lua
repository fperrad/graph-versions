#!/usr/bin/env lua

local http = require'socket.http'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return i, k, t[k]
        end
    end
end

local content = assert(http.request'http://ftp.gnu.org/gnu/gcc/')
local data = {}
for line in content:gmatch'([^\n]*)\n?' do
    local major, minor, patch, year, month, day = line:match'>gcc%-(%d+)%.(%d+)%.(%d+)/<[^>]+><[^>]+><[^>]+>(%d%d%d%d)%-(%d%d)%-(%d%d)'
    major = tonumber(major)
    if major and major >= 4 then
        if major == 4 then
            major = tonumber(tostring(major) .. '.' .. minor)
        else
            patch = minor .. '.' .. patch
        end
        if not data[major] then
            data[major] = {}
        end
        data[major][patch] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
end

local img = cp.new(1200, 480)
local start_date = 2005

local xTickLabels = {}
local yTickLabels = {}
for y, major, set in sorted_pairs(data) do
    local lbl
    if major < 5 then
        lbl = string.format('%.1f', major)
    else
        lbl = string.format('%-3s', tostring(major))
    end
    yTickLabels[y]= lbl
    local xdata = {}
    local ydata = {}
    local tags = {}
    local max_patch
    for _, patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - start_date] = tostring(year)
        xdata[#xdata+1] = date - start_date
        ydata[#ydata+1] = y
        tags[#tags+1] = false
        max_patch = patch
    end
    tags[#tags] = tostring(major) .. '.' .. tostring(max_patch)
    if major < 5 then
        tags[#tags] = string.format('%.1f.%d', major, max_patch)
    else
        tags[#tags] = string.format('%d.%s', major, max_patch)
    end
    img:setData(xdata, ydata, 'blue')
    img:setTag(tags)
end

img:setGraphOptions{
    title               = 'Releases of GCC',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    vertGraphOffset     = 15,
}

local f = assert(io.open('gcc.png', 'wb'))
f:write(img:draw())
f:close()
