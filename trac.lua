#!/usr/bin/env lua

local http = require'ssl.https'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return i, k, t[k]
        end
    end
end

local content = assert(http.request'https://download.edgewall.org/trac/')
local data = {}
for line in content:gmatch "([^\n]*)\n?" do
    local minor, patch, year, month, day
    minor, year, month, day = line:match'>[Tt]rac%-(0%.1%d)%.tar%.gz<[^>]+><[^>]+><[^>]+>(%d%d%d%d)%-(%d%d)%-(%d%d) '
    if minor then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][0] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
    minor, patch, year, month, day = line:match'>[Tt]rac%-(0%.1%d)%.(%d+)%.tar%.gz<[^>]+><[^>]+><[^>]+>(%d%d%d%d)%-(%d%d)%-(%d%d) '
    if minor then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][tonumber(patch)] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
    minor, year, month, day = line:match'>[Tt]rac%-(1%.[0246])%.tar%.gz<[^>]+><[^>]+><[^>]+>(%d%d%d%d)%-(%d%d)%-(%d%d) '
    if minor then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][0] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
    minor, patch, year, month, day = line:match'>[Tt]rac%-(1%.[0246])%.(%d+)%.tar%.gz<[^>]+><[^>]+><[^>]+>(%d%d%d%d)%-(%d%d)%-(%d%d) '
    if minor then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][tonumber(patch)] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
end

data['1.4'][0] = date{year = 2019, month = 8, day = 28}

local img = cp.new(1200, 480)
local start_date = 2006

local xTickLabels = {}
local yTickLabels = {}
for y, minor, set in sorted_pairs(data) do
    yTickLabels[y]= minor
    local xdata = {}
    local ydata = {}
    local tags = {}
    for _, patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - start_date] = tostring(year)
        xdata[#xdata+1] = date - start_date
        ydata[#ydata+1] = y
        local lbl = minor
        if patch > 0 then
            lbl = lbl .. '.' .. tostring(patch)
        end
        tags[#tags+1] = lbl
    end
    img:setData(xdata, ydata, 'blue up')
    img:setTag(tags)
end

img:setGraphOptions{
    title               = 'Releases of Trac',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    vertGraphOffset     = 15,
}

local f = assert(io.open('trac.png', 'wb'))
f:write(img:draw())
f:close()
