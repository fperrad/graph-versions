#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);
my $response = $ua->get('http://ftp.gnu.org/gnu/gcc/');
die $response->status_line unless $response->is_success;

my $gcc4 = {};
my $gcc = {};
for (split "\n", $response->decoded_content) {
    if (/>gcc-(\d+)\.(\d+)\.(\d+)\/<[^>]+><[^>]+><[^>]+>(\d{4})-(\d{2})-(\d{2})/) {
        next if $1 < 4;
        my $date = DateTime->new(
            year => $4,
            month => $5,
            day => $6,
        );
        if ($1 == 4) {
             $gcc4->{$2}->{$3} = $date;
        }
        else {
             $gcc->{$1}->{$2 . '.' . $3} = $date;
        }
    }
}

my $img = Chart::Plot::Tagged->new(1200, 480);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$gcc4}) {
    my $lbl = '4.' . $major;
    $yTickLabels{$y} = $lbl;
    my $set = $gcc4->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2005} = '' . $date->year;
        push @xdata, $date->year - 2005 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    $lbl = '4.' . $major . '.' . $max;
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, $lbl;
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
}
for my $major (sort {$a <=> $b} keys %{$gcc}) {
    my $lbl = $major . '  ';
    $yTickLabels{$y} = $lbl;
    my $set = $gcc->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2005} = '' . $date->year;
        push @xdata, $date->year - 2005 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    $lbl = $major . '.' . $max;
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, $lbl;
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of GCC 4',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'gcc.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

