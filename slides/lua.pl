#!/usr/bin/env perl

use strict;
use warnings;

use DateTime;
use Chart::Plot::Tagged;

my $lua = {
    #~ '4.0' => {
        #~ '0' => DateTime->new(year => 2000, month => 11, day =>  6),
        #~ '1' => DateTime->new(year => 2002, month =>  7, day =>  4),
    #~ },
    '5.0' => {
        '0' => DateTime->new(year => 2003, month =>  4, day => 11),
        '1' => DateTime->new(year => 2003, month => 11, day => 25),
        '2' => DateTime->new(year => 2004, month =>  3, day => 17),
        '3' => DateTime->new(year => 2006, month =>  6, day => 19),
    },
    '5.1' => {
        '0' => DateTime->new(year => 2006, month =>  2, day => 20),
        '1' => DateTime->new(year => 2006, month =>  6, day =>  7),
        '2' => DateTime->new(year => 2007, month =>  3, day => 29),
        '3' => DateTime->new(year => 2008, month =>  1, day => 21),
        '4' => DateTime->new(year => 2008, month =>  8, day => 18),
        '5' => DateTime->new(year => 2012, month =>  2, day => 13),
    },
    '5.2' => {
#        '0-work1' => DateTime->new(year => 2010, month =>  1, day =>  9),
#        '0-alpha' => DateTime->new(year => 2010, month => 11, day => 23),
#        '0-beta'  => DateTime->new(year => 2011, month =>  7, day =>  9),
        '0'       => DateTime->new(year => 2011, month => 12, day => 16),
        '1'       => DateTime->new(year => 2012, month =>  6, day =>  8),
        '2'       => DateTime->new(year => 2013, month =>  3, day => 21),
        '3'       => DateTime->new(year => 2013, month => 11, day => 11),
        '4'       => DateTime->new(year => 2015, month =>  2, day => 25),
    },
    '5.3' => {
#        '0-work1' => DateTime->new(year => 2013, month =>  7, day =>  5),
#        '0-work2' => DateTime->new(year => 2014, month =>  3, day => 21),
#        '0-work3' => DateTime->new(year => 2014, month =>  6, day => 19),
#        '0-alpha' => DateTime->new(year => 2014, month =>  7, day => 31),
#        '0-beta'  => DateTime->new(year => 2014, month => 10, day => 23),
        '0'       => DateTime->new(year => 2015, month =>  1, day =>  6),
        '1'       => DateTime->new(year => 2015, month =>  6, day => 10),
        '2'       => DateTime->new(year => 2015, month => 11, day => 25),
        '3'       => DateTime->new(year => 2016, month =>  5, day => 30),
        '4'       => DateTime->new(year => 2017, month =>  1, day => 12),
        '5'       => DateTime->new(year => 2018, month =>  6, day => 26),
    },
    '5.4' => {
        '0-work1' => DateTime->new(year => 2018, month =>  3, day => 13),
        '0-work2' => DateTime->new(year => 2018, month =>  6, day => 18),
    },
};

#my $img = Chart::Plot::Tagged->new(1200, 480);
#my $img = Chart::Plot::Tagged->new(800, 480);
my $img = Chart::Plot::Tagged->new(720, 320);

my %xTickLabels;
my %yTickLabels;
my $y = 0.5;
for my $major (sort {$a <=> $b} keys %{$lua}) {
    $yTickLabels{'' . $y} = $major;
    my $set = $lua->{$major};
    my @xdata;
    my @ydata;
    my @tags;
#    for my $minor (sort {$a <=> $b} keys %{$set}) {
    for my $minor (sort keys %{$set}) {
        my $date = $set->{$minor};
#        $xTickLabels{'' . $date->year - 2000} = '' . $date->year;
        push @xdata, $date->year - 2003 + $date->day_of_year/365;
        push @ydata, $y;
        my $lbl = $major;
        $lbl .= '.' . $minor if $minor ne '0' || $major ge '5.2';
        push @tags, $lbl;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, undef;
    $img->setData(\@xdata, \@ydata, 'blue up');
    $img->setTag(\@tags);
    $y++;
}
for my $y (0..15) {
    $xTickLabels{'' . $y} = 2003 + $y;
}

$img->setGraphOptions(
    title               => 'Releases of Lua',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'lua.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

