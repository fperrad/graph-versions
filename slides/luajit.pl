#!/usr/bin/env perl

use strict;
use warnings;

use DateTime;
use Chart::Plot::Tagged;

my $lua = {
    '1' => {
        '0.3' => DateTime->new(year => 2005, month =>  9, day =>  8),
        '1.0' => DateTime->new(year => 2006, month =>  3, day => 13),
        '1.2' => DateTime->new(year => 2006, month =>  6, day => 24),
        '1.3' => DateTime->new(year => 2007, month =>  5, day => 24),
        '1.4' => DateTime->new(year => 2008, month =>  2, day =>  5),
        '1.5' => DateTime->new(year => 2008, month => 10, day => 25),
        '1.6' => DateTime->new(year => 2010, month =>  3, day => 28),
        '1.7' => DateTime->new(year => 2011, month =>  5, day =>  5),
        '1.8' => DateTime->new(year => 2012, month =>  4, day => 16),
    },
    '2.0' => {
        '0-beta1'       => DateTime->new(year => 2009, month => 10, day => 31),
        '0-beta2'       => DateTime->new(year => 2009, month => 11, day =>  9),
        '0-beta3 x64'   => DateTime->new(year => 2010, month =>  3, day =>  7),
        '0-beta4'       => DateTime->new(year => 2010, month =>  3, day => 28),
        '0-beta5'       => DateTime->new(year => 2010, month =>  8, day => 24),
        '0-beta6 FFI'   => DateTime->new(year => 2011, month =>  2, day => 11),
        '0-beta7 ARM'   => DateTime->new(year => 2011, month =>  5, day =>  5),
        '0-beta8'       => DateTime->new(year => 2011, month =>  6, day => 23),
        '0-beta9 PPC'   => DateTime->new(year => 2011, month => 12, day => 14),
        '0-beta10 MIPS' => DateTime->new(year => 2012, month =>  5, day =>  9),
        '0-beta11'      => DateTime->new(year => 2012, month => 10, day => 16),
#        '0-rc1'         => DateTime->new(year => 2012, month => 10, day => 31),
#        '0-rc2'         => DateTime->new(year => 2012, month => 11, day =>  6),
#        '0-rc3'         => DateTime->new(year => 2012, month => 11, day =>  8),
        '0'             => DateTime->new(year => 2012, month => 11, day => 12),
        '1'             => DateTime->new(year => 2013, month =>  2, day => 19),
        '2'             => DateTime->new(year => 2013, month =>  6, day =>  3),
        '3 PS4'         => DateTime->new(year => 2014, month =>  3, day => 12),
        '4 PS Vita'     => DateTime->new(year => 2015, month =>  5, day => 14),
        '5'             => DateTime->new(year => 2017, month =>  5, day =>  1),
    },
    '2.1' => {
        '0-beta1'       => DateTime->new(year => 2015, month =>  8, day => 25),
        '0-beta2'       => DateTime->new(year => 2016, month =>  3, day =>  3),
        '0-beta3 ARM64' => DateTime->new(year => 2017, month =>  5, day =>  1),
    },
};

#my $img = Chart::Plot::Tagged->new(1200, 480);
my $img = Chart::Plot::Tagged->new(600, 300);

my %xTickLabels;
my %yTickLabels;
my $y = 0.2;
#for my $major (sort {$a <=> $b} keys %{$lua}) {
for my $major (sort keys %{$lua}) {
    $yTickLabels{'' . $y} = $major;
    my $set = $lua->{$major};
    my @xdata;
    my @ydata;
    my @tags;
#    for my $minor (sort {$a <=> $b} keys %{$set}) {
    for my $minor (sort keys %{$set}) {
        my $date = $set->{$minor};
        if ($date->year >= 2009) {
            $xTickLabels{'' . $date->year - 2009} = '' . $date->year;
            push @xdata, $date->year - 2009 + $date->day_of_year/365;
            push @ydata, $y;
            my $lbl = $major;
            $lbl .= '.' . $minor;
            push @tags, $lbl;
        }
        else {
            my @xdata2 = (0, 2);
            my @ydata2 = ($y, $y);
            my @tags2 = (undef, undef);
            $img->setData(\@xdata2, \@ydata2, 'blue nopoints');
            $img->setTag(\@tags2);
        }
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, undef;
    $img->setData(\@xdata, \@ydata, 'blue up');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of LuaJIT',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'luajit.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

