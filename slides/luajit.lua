#!/usr/bin/env lua

local cp = require 'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return k, t[k]
        end
    end
end

local data = {
    ['1'] = {
        ['0.3']           = date{year = 2005, month =  9, day =  8},
        ['1.0']           = date{year = 2006, month =  3, day = 13},
        ['1.2']           = date{year = 2006, month =  6, day = 24},
        ['1.3']           = date{year = 2007, month =  5, day = 24},
        ['1.4']           = date{year = 2008, month =  2, day =  5},
        ['1.5']           = date{year = 2008, month = 10, day = 25},
        ['1.6']           = date{year = 2010, month =  3, day = 28},
        ['1.7']           = date{year = 2011, month =  5, day =  5},
        ['1.8']           = date{year = 2012, month =  4, day = 16},
    },
    ['2.0'] = {
        ['0-beta1']       = date{year = 2009, month = 10, day = 31},
        ['0-beta2']       = date{year = 2009, month = 11, day =  9},
        ['0-beta3 x64']   = date{year = 2010, month =  3, day =  7},
        ['0-beta4']       = date{year = 2010, month =  3, day = 28},
        ['0-beta5']       = date{year = 2010, month =  8, day = 24},
        ['0-beta6 FFI']   = date{year = 2011, month =  2, day = 11},
        ['0-beta7 ARM']   = date{year = 2011, month =  5, day =  5},
        ['0-beta8']       = date{year = 2011, month =  6, day = 23},
        ['0-beta9 PPC']   = date{year = 2011, month = 12, day = 14},
        ['0-beta10 MIPS'] = date{year = 2012, month =  5, day =  9},
        ['0-beta11']      = date{year = 2012, month = 10, day = 16},
--~         ['0-rc1']         = date{year = 2012, month = 10, day = 31},
--~         ['0-rc2']         = date{year = 2012, month = 11, day =  6},
--~         ['0-rc3']         = date{year = 2012, month = 11, day =  8},
        ['0']             = date{year = 2012, month = 11, day = 12},
        ['1']             = date{year = 2013, month =  2, day = 19},
        ['2']             = date{year = 2013, month =  6, day =  3},
        ['3 PS4']         = date{year = 2014, month =  3, day = 12},
        ['4 PS Vita']     = date{year = 2015, month =  5, day = 14},
        ['5']             = date{year = 2017, month =  5, day =  1},
    },
    ['2.1'] = {
        ['0-beta1']       = date{year = 2015, month =  8, day = 25},
        ['0-beta2']       = date{year = 2016, month =  3, day =  3},
        ['0-beta3 ARM64'] = date{year = 2017, month =  5, day =  1},
    },
}

local img = cp.new(600, 300)

local xTickLabels = {}
local yTickLabels = {}
local y = 0.2
for major, set in sorted_pairs(data) do
    yTickLabels[y]= major
    local xdata = {}
    local ydata = {}
    local tags = {}
    for minor, date in sorted_pairs(set) do
        local year = math.floor(date)
        if year >= 2009 then
            xTickLabels[year - 2009] = tostring(year)
            xdata[#xdata+1] = date - 2009
            ydata[#ydata+1] = y
            tags[#tags+1] = major .. '.' .. minor
        else
            local xdata2 = {0, 2}
            local ydata2 = {y, y}
            img:setData(xdata2, ydata2, 'blue nopoints')
        end
    end
    img:setData(xdata, ydata, 'blue up')
    img:setTag(tags)
    y = y + 1
end

img:setGraphOptions{
    title               = 'Releases of LuaJIT',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
}

local f = assert(io.open('luajit.png', 'wb'))
f:write(img:draw())
f:close()
