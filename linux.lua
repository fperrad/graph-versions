#!/usr/bin/env lua

local http = require'ssl.https'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return i, k, t[k]
        end
    end
end

local months = {
    Jan = 1,
    Feb = 2,
    Mar = 3,
    Apr = 4,
    May = 5,
    Jun = 6,
    Jul = 7,
    Aug = 8,
    Sep = 9,
    Oct = 10,
    Nov = 11,
    Dec = 12,
}

local codename = {      -- some Debian, Ubuntu & RHEL
    ['2.6.32'] = '6.0 Squeeze, 10.04 Lucid, RHEL 6',
    ['3.2']    = '7 Wheezy, 12.04 Precise',
    ['3.10']   = 'RHEL 7',
    ['3.13']   = '14.04 Trusty',
    ['3.16']   = '8 Jessie',
    ['4.4']    = '16.04 Xenial',
    ['4.9']    = '9 Stretch',
    ['4.15']   = '18.04 Bionic',
    ['4.18']   = 'RHEL 8',
    ['4.19']   = '10 Buster',
    ['5.4']    = '20.04 Focal',
    ['5.10']   = '11 Bullseye',
    ['5.14']   = 'RHEL 9',
    ['5.15']   = '22.04 Jammy',
    ['6.1']    = '12 Bookworm',
    ['6.8']    = '24.04 Noble',
}

local data = {}
for _, url in ipairs{ 'https://mirrors.edge.kernel.org/pub/linux/kernel/v2.6/',
                      'https://mirrors.edge.kernel.org/pub/linux/kernel/v2.6/longterm/v2.6.27/',
                      'https://mirrors.edge.kernel.org/pub/linux/kernel/v2.6/longterm/v2.6.32/',
                      'https://mirrors.edge.kernel.org/pub/linux/kernel/v2.6/longterm/v2.6.33/',
                      'https://mirrors.edge.kernel.org/pub/linux/kernel/v2.6/longterm/v2.6.34/',
                      'https://mirrors.edge.kernel.org/pub/linux/kernel/v2.6/longterm/v2.6.35/',
                      'https://mirrors.edge.kernel.org/pub/linux/kernel/v3.x/',
                      'https://mirrors.edge.kernel.org/pub/linux/kernel/v4.x/',
                      'https://mirrors.edge.kernel.org/pub/linux/kernel/v5.x/',
                      'https://mirrors.edge.kernel.org/pub/linux/kernel/v6.x/' } do
    print(url)
    local content = assert(http.request(url))
    for line in content:gmatch'([^\n]*)\n?' do
        local minor, patch, release, day, month, year, _
        patch, day, month, year, _ = line:match'>linux%-2%.6%.(%d+)%.tar%.gz</a>%s+(%d%d)%-(%w%w%w)%-(%d%d%d%d)%s+%d%d:%d%d%s+(%d+)'
        if patch and tonumber(patch) >= 27 then
            minor = '2.6.' .. patch
            if not data[minor] then
               data[minor] = {}
            end
            data[minor][-1] = date{year = tonumber(year), month = months[month], day = tonumber(day)}
        end
        patch, release, day, month, year, _ = line:match'>linux%-2%.6%.(%d+)%.(%d+)%.tar%.gz</a>%s+(%d%d)%-(%w%w%w)%-(%d%d%d%d)%s+%d%d:%d%d%s+(%d+)'
        if patch and tonumber(patch) >= 27 then
            minor = '2.6.' .. patch
            if not data[minor] then
               data[minor] = {}
            end
            data[minor][tonumber(release)] = date{year = tonumber(year), month = months[month], day = tonumber(day)}
        end
        minor, patch, day, month, year, _ = line:match'>linux%-([3-6])%.(%d+)%.tar%.gz</a>%s+(%d%d)%-(%w%w%w)%-(%d%d%d%d)%s+%d%d:%d%d%s+(%d+)'
        if minor then
            minor = minor .. string.format('.%02d', tonumber(patch))
            if not data[minor] then
               data[minor] = {}
            end
            data[minor][-1] = date{year = tonumber(year), month = months[month], day = tonumber(day)}
        end
        minor, patch, release, day, month, year, _ = line:match'>linux%-([3-6])%.(%d+)%.(%d+)%.tar%.gz</a>%s+(%d%d)%-(%w%w%w)%-(%d%d%d%d)%s+%d%d:%d%d%s+(%d+)'
        if minor then
            minor = minor .. string.format('.%02d', tonumber(patch))
            if not data[minor] then
               data[minor] = {}
            end
            data[minor][tonumber(release)] = date{year = tonumber(year), month = months[month], day = tonumber(day)}
        end
    end
end

local img = cp.new(1200, 800)

local xTickLabels = {}
local yTickLabels = {}
local xcodename = {}
local ycodename = {}
local tcodename = {}
for y, minor, set in sorted_pairs(data) do
    local lbl = minor:gsub('%.0(%d)', '.%1')
    if codename[lbl] then
        local _, _, date = sorted_pairs(set)()
        xcodename[#xcodename+1] = date - 2008
        ycodename[#ycodename+1] = y
        tcodename[#tcodename+1] = codename[lbl]
    end
    yTickLabels[y]= string.format('%-6s', lbl)
    local xdata = {}
    local ydata = {}
    local tags = {}
    local max_patch
    for _, patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - 2008] = tostring(year)
        xdata[#xdata+1] = date - 2008
        ydata[#ydata+1] = y
        tags[#tags+1] = false
        max_patch = patch
    end
    lbl = minor:gsub('%.0(%d)', '.%1')
    if max_patch > 0 then
        lbl = lbl .. '.' .. tostring(max_patch)
    end
    tags[#tags] = lbl
    img:setData(xdata, ydata, 'blue')
    img:setTag(tags)
end
img:setData(xcodename, ycodename, 'nopoints nolines blue up')
img:setTag(tcodename)

img:setGraphOptions{
    title               = 'Releases of Linux',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    horGraphOffset      = 75,
    vertGraphOffset     = 15,
}

local f = assert(io.open('linux.png', 'wb'))
f:write(img:draw())
f:close()
