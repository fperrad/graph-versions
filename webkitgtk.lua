#!/usr/bin/env lua

local http = require'ssl.https'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return i, k, t[k]
        end
    end
end

local content = assert(http.request'https://webkitgtk.org/releases/')
local data = {}
for line in content:gmatch'([^\n]*)\n?' do
    local minor, patch, year, month, day = line:match'>webkitgtk%-2%.(%d+)%.(%d+)%.tar%.[gx]z<[^>]+><[^>]+><[^>]+>(%d%d%d%d)%-(%d%d)%-(%d%d)%s+'
    minor = tonumber(minor)
    if minor and (minor % 2) == 0 then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][tonumber(patch)] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
end
data[2] = { [0] = date{year = 2013, month =  9, day = 27},
            [1] = date{year = 2013, month = 10, day = 16},
            [2] = date{year = 2013, month = 11, day = 11},
            [3] = date{year = 2013, month = 12, day = 18},
            [4] = date{year = 2014, month =  1, day = 21},
            [5] = date{year = 2014, month =  2, day = 19},
            [6] = date{year = 2014, month =  3, day = 19},
            [7] = date{year = 2014, month =  4, day = 30},
            [8] = date{year = 2014, month = 10, day =  1} }
data[4][0] = date{year = 2014, month =  3, day = 24}
data[4][1] = date{year = 2014, month =  4, day = 14}
data[4][2] = date{year = 2014, month =  5, day = 12}
data[4][3] = date{year = 2014, month =  5, day = 26}
data[4][4] = date{year = 2014, month =  7, day =  8}
data[4][5] = date{year = 2014, month =  8, day = 26}
data[4][6] = date{year = 2014, month =  9, day = 29}

local img = cp.new(1200, 480)
local start_date = 2013

local xTickLabels = {}
local yTickLabels = {}
for y, minor, set in sorted_pairs(data) do
    local lbl = '2.' .. tostring(minor)
    yTickLabels[y]= string.format('%-4s', lbl)
    local xdata = {}
    local ydata = {}
    local tags = {}
    local max_patch
    for _, patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - start_date] = tostring(year)
        xdata[#xdata+1] = date - start_date
        ydata[#ydata+1] = y
        tags[#tags+1] = false
        max_patch = patch
    end
    tags[#tags] = lbl .. '.' .. tostring(max_patch)
    img:setData(xdata, ydata, 'blue')
    img:setTag(tags)
end

img:setGraphOptions{
    title               = 'Releases of WebKitGtk',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
--    horGraphOffset      = 75,
    vertGraphOffset     = 15,
}

local f = assert(io.open('webkitgtk.png', 'wb'))
f:write(img:draw())
f:close()
