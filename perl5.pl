#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);
my $response = $ua->get('https://raw.githubusercontent.com/Perl/perl5/blead/pod/perlhist.pod');
die $response->status_line unless $response->is_success;

my %month = (
    Jan => 1,
    Feb => 2,
    Mar => 3,
    Apr => 4,
    May => 5,
    Jun => 6,
    Jul => 7,
    Aug => 8,
    Sep => 9,
    Oct => 10,
    Nov => 11,
    Dec => 12,
);

my $perl = {};
for (split "\n", $response->decoded_content) {
    last if /^=head2 SELECTED/;
    if (/5\.(\d+)\.(\d+)\s+(\d{4})-(\w{3})-(\d{2})/) {
        next unless $1 % 2 == 0;
        my $date = DateTime->new(
            year => $3,
            month => $month{$4},
            day => $5,
        );
        $perl->{$1}->{$2} = $date;
    }
}

my $img = Chart::Plot::Tagged->new(1200, 900);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$perl}) {
    my $lbl = '5.' . $major;
    $lbl .= ' ' if length $lbl < 4;
    $yTickLabels{$y} = $lbl;
    my $set = $perl->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2000} = '' . $date->year;
        push @xdata, $date->year - 2000 + $date->day_of_year/365;
        push @ydata, $y;
        my $tag = '5.' . $major . '.' . $minor;
        push @tags, $tag unless $tag eq '5.18.3';
    }
    if ($major <= 8) {
        my @xdata2 = ($xdata[-2], $xdata[-1]);
        my @ydata2 = ($y, $y);
        my @tags2 = (undef, $tags[-1]);
        $img->setData(\@xdata2, \@ydata2, 'blue dashedline up');
        $img->setTag(\@tags2);
        pop @xdata;
        pop @ydata;
        pop @tags;
    }
    else {
        push @xdata, $xdata[-1] + 0.001;
        push @ydata, $ydata[-1];
        push @tags, undef;
    }
    $img->setData(\@xdata, \@ydata, 'blue up');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of Perl 5',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    horGraphOffset      => 40,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'perl5.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;
