#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);
my $response = $ua->get('https://www.musl-libc.org/releases/');
die $response->status_line unless $response->is_success;

my %month = (
    Jan => 1,
    Feb => 2,
    Mar => 3,
    Apr => 4,
    May => 5,
    Jun => 6,
    Jul => 7,
    Aug => 8,
    Sep => 9,
    Oct => 10,
    Nov => 11,
    Dec => 12,
);

my $musl = {};
for (split "\n", $response->decoded_content) {
    if (/(\w{3})\s+(\d+)\s+([\d:]{4,5})\s+<[^>]+>musl-(\d)\.(\d+)\.(\d+).tar.gz<[^>]+>/) {
        next if $4 == 0 && $5 < 6;
        my $month = $month{$1};
        my $day = $2;
        my $year = $3;
        if (index($year, ':') > 0) {
            my @now = gmtime(time);
            $year = 1900 + $now[5];
            $year-- if $month > $now[4]+1;
        }
        my $date = DateTime->new(
            year => $year,
            month => $month,
            day => $day,
        );
        $musl->{$4 . '.' . $5}->{$6} = $date;
    }
}

my $img = Chart::Plot::Tagged->new(1200, 480);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$musl}) {
    $yTickLabels{$y} = $major;
    my $set = $musl->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 2011} = '' . $date->year;
        push @xdata, $date->year - 2011 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, $major . '.' . $max;
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
}

$img->setGraphOptions(
    title               => 'Releases of musl',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'musl.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;

