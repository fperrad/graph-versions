#!/usr/bin/env lua

local http = require'socket.http'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return i, k, t[k]
        end
    end
end

local content = assert(http.request'http://ftp.gnu.org/gnu/binutils/')
local data = {}
for line in content:gmatch'([^\n]*)\n?' do
    local minor, patch, year, month, day
    minor, year, month, day = line:match'>binutils%-2%.(%d+)%.tar%.gz<[^>]+><[^>]+><[^>]+>(%d%d%d%d)%-(%d%d)%-(%d%d)'
    minor = tonumber(minor)
    if minor and minor >= 10 then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][0] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
    minor, patch, year, month, day = line:match'>binutils%-2%.(%d+)%.(%d+)%.tar%.gz<[^>]+><[^>]+><[^>]+>(%d%d%d%d)%-(%d%d)%-(%d%d)'
    minor = tonumber(minor)
    if minor and minor >= 10 then
        if not data[minor] then
            data[minor] = {}
        end
        data[minor][tonumber(patch)] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
    end
end
local unk = 15
data[15] = { [0] = date{year = 2004, month =  5, day = unk} }
data[16] = { [0] = date{year = 2005, month =  5, day = unk},
             [1] = date{year = 2005, month =  6, day = unk} }
data[17] = { [0] = date{year = 2006, month =  6, day = unk} }
data[18] = { [0] = date{year = 2007, month =  8, day = unk} }
data[19] = { [0] = date{year = 2008, month = 10, day = 27},
             [1] = date{year = 2009, month =  2, day =  2} }
data[20] = { [0] = date{year = 2009, month = 10, day = 19},
             [1] = date{year = 2010, month =  3, day =  8} }
data[21] = { [0] = date{year = 2010, month = 12, day =  9},
             [1] = date{year = 2011, month =  6, day = 27} }

local img = cp.new(1200, 480)
local start_date = 2000

local xTickLabels = {}
local yTickLabels = {}
for y, minor, set in sorted_pairs(data) do
    yTickLabels[y]= '2.' .. tostring(minor)
    local xdata = {}
    local ydata = {}
    local tags = {}
    local max_patch
    for _, patch, date in sorted_pairs(set) do
        local year = math.floor(date)
        xTickLabels[year - start_date] = tostring(year)
        xdata[#xdata+1] = date - start_date
        ydata[#ydata+1] = y
        tags[#tags+1] = false
        max_patch = patch
    end
    local lbl = '2.' .. tostring(minor)
    if max_patch > 0 then
        lbl = lbl .. '.' .. tostring(max_patch)
    end
    tags[#tags] = lbl
    img:setData(xdata, ydata, 'blue')
    img:setTag(tags)
end

img:setGraphOptions{
    title               = 'Releases of binutils',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    vertGraphOffset     = 15,
}

local f = assert(io.open('binutils.png', 'wb'))
f:write(img:draw())
f:close()
