#!/usr/bin/env lua

local lfs = require'lfs'
local cp = require'ChartPlot'

local function date (t)
    local n = os.time(t) - os.time{year = t.year, month = 1, day = 1}
    local d = os.time{year = t.year+1, month = 1, day = 1} - os.time{year = t.year, month = 1, day = 1}
    return t.year + n / d
end

local function sorted_pairs (t)
    local keys = {}
    for k in pairs(t) do
        keys[#keys+1] = k
    end
    table.sort(keys)
    local i = 0
    return function ()
        i = i + 1
        local k = keys[i]
        if k then
            return k, t[k]
        end
    end
end

if not lfs.attributes'rollup' then
    os.execute'git clone https://github.com/rollup/rollup.git'
else
    os.execute'cd rollup && git pull --no-rebase'
end

local data = {}
for line in io.popen'cd rollup && git log --tags --pretty=format:"%ai %d"':lines() do
    local year, month, day, tags = line:match'^(%d%d%d%d)%-(%d%d)%-(%d%d) %d%d:%d%d:%d%d [^ ]+%s+%(([^)]+)%)'
    if tags then
        for tag in tags:gmatch'([^,]*),?' do
            local major, minor, patch = tag:match'tag: v(%d)%.(%d+)%.(%d+)$'
            major = tonumber(major)
            minor = tonumber(minor)
            patch = tonumber(patch)
            if major and minor and patch then
                if not data[major] then
                    data[major] = {}
                end
                if not data[major][minor] then
                    data[major][minor] = {}
                end
                data[major][minor][patch] = date{year = tonumber(year), month = tonumber(month), day = tonumber(day)}
            end
        end
    end
end

local img = cp.new(1200, 800)
local start_date = 2018

local xTickLabels = {}
local yTickLabels = {}
for major = 1, #data do
    for minor, set in sorted_pairs(data[major]) do
        local y = 1 + minor
        yTickLabels[y]= ''
        local xdata = {}
        local ydata = {}
        local tags = {}
        local max_patch
        for patch, date in sorted_pairs(set) do
            local year = math.floor(date)
            xTickLabels[year - start_date] = tostring(year)
            xdata[#xdata+1] = date - start_date
            ydata[#ydata+1] = y
            tags[#tags+1] = false
            max_patch = patch
        end
        tags[#tags] = tostring(major) .. '.' .. tostring(minor) .. '.' .. tostring(max_patch)
        img:setData(xdata, ydata, 'blue')
        img:setTag(tags)
    end
end

img:setGraphOptions{
    title               = 'Releases of Rollup',
    horAxisLabel        = 'year',
    xTickLabels         = xTickLabels,
    yTickLabels         = yTickLabels,
    vertGraphOffset     = 15,
}

local f = assert(io.open('rollup.png', 'wb'))
f:write(img:draw())
f:close()
