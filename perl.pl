#!/usr/bin/env perl

use strict;
use warnings;

use LWP::UserAgent;
use DateTime;
use Chart::Plot::Tagged;

my $ua = LWP::UserAgent->new();
$ua->show_progress(1);
my $response = $ua->get('https://raw.githubusercontent.com/Perl/perl5/blead/pod/perlhist.pod');
die $response->status_line unless $response->is_success;

my %month = (
    Jan => 1,
    Feb => 2,
    Mar => 3,
    Apr => 4,
    May => 5,
    Jun => 6,
    Jul => 7,
    Aug => 8,
    Sep => 9,
    Oct => 10,
    Nov => 11,
    Dec => 12,
);

my $perl = {};
my $perl5 = {};
for (split "\n", $response->decoded_content) {
    last if /^=head2 SELECTED/;
    if (/([1-4])\.(0\d\d)\s+(\d{4})-(\w{3})-(\d{2})/
     or /([1-4])\.0\d\d\.\.(\d+)\s+(\d{4})-(\w{3})-(\d{2})/) {
        my $date = DateTime->new(
            year => $3,
            month => $month{$4},
            day => $5,
        );
        $perl->{$1}->{$2} = $date;
    }
    if (/5\.(0\d\d)(\w|_[0-2]\d)?\s+(\d{4})-(\w{3})-(\d{2})/) {
        my $date = DateTime->new(
            year => $3,
            month => $month{$4},
            day => $5,
        );
        $perl5->{$1}->{$2||'0'} = $date;
    }
    if (/5\.(\d+)\.(\d+)\s+(\d{4})-(\w{3})-(\d{2})/) {
        next if $1 < 6 or $1 % 2 != 0;
        my $date = DateTime->new(
            year => $3,
            month => $month{$4},
            day => $5,
        );
        $perl5->{$1}->{$2} = $date;
    }
}

my $img = Chart::Plot::Tagged->new(1200, 480);

my %xTickLabels;
my %yTickLabels;
my $y = 1;
for my $major (sort {$a <=> $b} keys %{$perl}) {
    my $lbl = $major . '    ';
    $yTickLabels{$y} = $lbl;
    my $set = $perl->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort {$a <=> $b} keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 1987} = '' . $date->year;
        push @xdata, $date->year - 1987 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    push @xdata, $xdata[-1] + 0.001;
    push @ydata, $ydata[-1];
    push @tags, sprintf "%d.%03d", $major, $max;
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
    $y++;
}
for my $major (sort {$a <=> $b} keys %{$perl5}) {
    my $lbl = '5.' . $major;
    $lbl .= ' ' if length $lbl < 4;
    $lbl .= ' ' if length $lbl < 5;
    $yTickLabels{$y} = $lbl;
    my $set = $perl5->{$major};
    my @xdata;
    my @ydata;
    my @tags;
    my $max;
    for my $minor (sort keys %{$set}) {
        my $date = $set->{$minor};
        $xTickLabels{'' . $date->year - 1987} = '' . $date->year;
        push @xdata, $date->year - 1987 + $date->day_of_year/365;
        push @ydata, $y;
        push @tags, undef;
        $max = $minor;
    }
    my $tag;
    if ($major < 6) {
        $tag = '5.' . $major . $max
    }
    else {
        $tag = '5.' . $major . '.' . $max;
    }
    if ($major >= 4 && $major <= 8) {
        my @xdata2 = ($xdata[-2], $xdata[-1]);
        my @ydata2 = ($y, $y);
        my @tags2 = (undef, $tag);
        $img->setData(\@xdata2, \@ydata2, 'blue dashedline');
        $img->setTag(\@tags2);
        pop @xdata;
        pop @ydata;
        pop @tags;
    }
    else {
        push @xdata, $xdata[-1] + 0.001;
        push @ydata, $ydata[-1];
        push @tags, $tag;
    }
    $img->setData(\@xdata, \@ydata, 'blue');
    $img->setTag(\@tags);
    $y++;
    $y++ if $major >= 5 && $major < 12;
}


$img->setGraphOptions(
    title               => 'Releases of Perl',
    horAxisLabel        => 'year',
    xTickLabels         => \%xTickLabels,
    yTickLabels         => \%yTickLabels,
    horGraphOffset      => 50,
    vertGraphOffset     => 15,
);

open my $fh, '>', 'perl.png' or die $!;
binmode $fh;
print {$fh} $img->draw;
close $fh;
